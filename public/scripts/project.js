/* global mergeRequest:false */
/* global utils:false */
/* global api:false */
/* global popup:false */

function getProjectsError(t, error) {
  throw t.NotHandled('error occurred when getting projects from GitLab API', error);
}

function getProjects(t) {
  return utils.getAuthToken(t)
    .then(api.getAllProjects)
    .catch(getProjectsError.bind(this, t));
}

function mapProjects(p) {
  return {
    text: p.name_with_namespace,
    callback: function selectProject(t) {
      return mergeRequest.showProjectMergeRequest(t, p.id, p.name_with_namespace);
    }
  };
}

function showProjectsCallback(response) {
  return response.data.map(mapProjects);
}

function showProjectsError(t, error) {
  throw t.NotHandled('error occurred while displaying projects', error);
}

function showProjects(t) {
  return getProjects(t)
    .then(showProjectsCallback)
    .then(popup.openProjects.bind(this, t))
    .catch(showProjectsError.bind(this, t));
}

window.project = {
  showProjects: showProjects
};
